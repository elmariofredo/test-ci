# test-ci

> You cannot override variables using local `gitlab-runner exec`

1. Fail

       08:21:39 in test-ci on  master 
       ➜ gitlab-runner exec docker fail --env TEST_VAR="0"
       Runtime platform                                           arch=amd64 os=darwin pid=85155 revision=8bb608ff        version=11.7.0
       Running with gitlab-runner 11.7.0 (8bb608ff)
       Using Docker executor with image alpine ...
       Pulling docker image alpine ...
       Using docker image        sha256:caf27325b298a6730837023a8a342699c8b7b388b8d87       8966b064a1320043019 for alpine ...
       Running on runner--project-0-concurrent-0 via        Marios-MacBook-Pro-2.local...
       Cloning repository...
       Cloning into '/builds/project-0'...
       done.
       Checking out 5e6cc42f as master...
       Skipping Git submodules setup
       $ echo "TEST_VAR: $TEST_VAR"
       TEST_VAR: 1
       $ exit $TEST_VAR
       ERROR: Job failed: exit code 1
       FATAL: exit code 1                                 
       
       08:21:59 in test-ci on  master took 16s 

It will fail as variable in the job will take precedence ( using top root `variables` has same effect )

2. Pass

       08:21:59 in test-ci on  master took 16s 
       ➜ gitlab-runner exec docker pass --env TEST_VAR="0"
       Runtime platform                                    arch=amd64 os=darwin pid=85272 revision=8bb608ff version=11.7.0
       Running with gitlab-runner 11.7.0 (8bb608ff)
       Using Docker executor with image alpine ...
       Pulling docker image alpine ...
       Using docker image sha256:caf27325b298a6730837023a8a342699c8b7b388b8d878966b064a1320043019 for alpine ...
       Running on runner--project-0-concurrent-0 via Marios-MacBook-Pro-2.local...
       Cloning repository...
       Cloning into '/builds/project-0'...
       done.
       Checking out 5e6cc42f as master...
       Skipping Git submodules setup
       $ echo "TEST_VAR: $TEST_VAR"
       TEST_VAR: 0
       $ exit $TEST_VAR
       Job succeeded

       08:22:24 in test-ci on  master took 18s 

when variable is not present inside of .gitlab-ci.yml job works as it should
